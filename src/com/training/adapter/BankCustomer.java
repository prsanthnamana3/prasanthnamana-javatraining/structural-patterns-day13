package com.training.adapter;

import java.util.Scanner;

public class BankCustomer implements CreditCard{

    private  BankDetails bankDetails;

    public BankCustomer(BankDetails bankDetails){
        this.bankDetails = bankDetails;
    }

    @Override
    public void getBankDetails() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the Bank Name name :");
            String bankName = scanner.nextLine();
            System.out.println("Enter the account holder name :");
            String ownerName = scanner.nextLine();
            System.out.println("Enter the account number : ");
            long accNo = scanner.nextLong();
            bankDetails.setBankName(bankName);
            bankDetails.setAccNumber(accNo);
            bankDetails.setAccountOwnerName(ownerName);

        }
        catch (Exception e){}

    }

    @Override
    public String getCreditCard() {
        return ("The Account number "+bankDetails.getAccNumber()+" of "+bankDetails.getAccountOwnerName()+
                " in "+bankDetails.getBankName()+ " bank is valid and authenticated for issuing the credit"+
        "card. ");
    }
}
