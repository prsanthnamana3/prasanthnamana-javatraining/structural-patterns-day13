package com.training.adapter;

public class Demo {
    public static void main(String[] args){
        CreditCard cc =  new BankCustomer(new BankDetails());
        cc.getBankDetails();
        System.out.println(cc.getCreditCard());

        cc =  new BankCustomer1();
        cc.getBankDetails();
        System.out.println(cc.getCreditCard());
    }
}
