package com.training.adapter;

public interface CreditCard {
    void getBankDetails();
    String getCreditCard();
}
