package com.training.adapter;

import java.util.Scanner;

public class BankCustomer1 extends  BankDetails implements CreditCard{



    @Override
    public void getBankDetails() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the Bank Name name :");
            String bankName = scanner.nextLine();
            System.out.println("Enter the account holder name :");
            String ownerName = scanner.nextLine();
            System.out.println("Enter the account number : ");
            long accNo = scanner.nextLong();
            setBankName(bankName);
            setAccNumber(accNo);
            setAccountOwnerName(ownerName);

        }
        catch (Exception e){}

    }

    @Override
    public String getCreditCard() {
        return ("The Account number "+getAccNumber()+" of "+getAccountOwnerName()+
                " in "+getBankName()+ " bank is valid and authenticated for issuing the credit"+
                "card. ");
    }
}
